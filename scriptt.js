var grocery = [
    {
        "name": "Tofu",
        "category": "dairy",
        "price": 80,
        "quantity": 0
    },
    {
        "name": "Cottage Cheez",
        "category": "dairy",
        "price": 70,
        "quantity": 0
    },
    {
        "name": "Milk",
        "category": "dairy",
        "price": 30,
        "quantity": 0
    },
    {
        "name": "Egg",
        "category": "dairy",
        "price": 4,
        "quantity": 0
    },
    {
        "name": "Chicken",
        "category": "dairy",
        "price": 250,
        "quantity": 0
    },
    {
        "name": "Beef",
        "category": "dairy",
        "price": 230,
        "quantity": 0
    },
    {
        "name": "Butter",
        "category": "dairy",
        "price": 50,
        "quantity": 0
    },
    {
        "name": "Cheez",
        "category": "dairy",
        "price": 60,
        "quantity": 0
    },
    {
        "name": "Curd",
        "category": "dairy",
        "price": 40,
        "quantity": 0
    },
    {
        "name": "Yogurt",
        "category": "dairy",
        "price": 30,
        "quantity": 0
    },

    {
        "name": "Walnut",
        "category": "dryfruit",
        "price": 400,
        "quantity": 0
    },
    {
        "name": "Almond",
        "category": "dryfruit",
        "price": 300,
        "quantity": 0
    },
    {
        "name": "Fennel",
        "category": "dryfruit",
        "price": 50,
        "quantity": 0
    },
    {
        "name": "Flax Seed",
        "category": "dryfruit",
        "price": 200,
        "quantity": 0
    },
    {
        "name": "Cashew",
        "category": "dryfruit",
        "price": 250,
        "quantity": 0
    },
    {
        "name": "Coconut",
        "category": "dryfruit",
        "price": 100,
        "quantity": 0
    },
    {
        "name": "Raisin",
        "category": "dryfruit",
        "price": 250,
        "quantity": 0
    },
    {
        "name": "Dates",
        "category": "dryfruit",
        "price": 250,
        "quantity": 0
    },
    {
        "name": "Peanut",
        "category": "dryfruit",
        "price": 100,
        "quantity": 0
    },
    {
        "name": "Lotus Seed",
        "category": "dryfruit",
        "price": 50,
        "quantity": 0
    },
    {
        "name": "Pistachio",
        "category": "dryfruit",
        "price": 150,
        "quantity": 0
    },
    {
        "name": "Apple",
        "category": "Fruit",
        "price": 100,
        "quantity": 0

    },
    {
        "name": "Mango",
        "category": "Fruit",
        "price": 50,
        "quantity": 0

    },
    {
        "name": "Banana",
        "category": "Fruit",
        "price": 30,
        "quantity": 0

    },
    {
        "name": "Papaya",
        "category": "Fruit",
        "price": 35,
        "quantity": 0

    },
    {
        "name": "Grapes",
        "category": "Fruit",
        "price": 45,
        "quantity": 0

    },
    {
        "name": "Pomegranate",
        "category": "Fruit",
        "price": 50,
        "quantity": 0

    },
    {
        "name": "Plum",
        "category": "Fruit",
        "price": 30,
        "quantity": 0

    },
    {
        "name": "Cherry",
        "category": "Fruit",
        "price": 35,
        "quantity": 0

    },
    {
        "name": "Guava",
        "category": "Fruit",
        "price": 40,
        "quantity": 0

    },
    {
        "name": "Melon",
        "category": "Fruit",
        "price": 50,
        "quantity": 0

    },

    {
        "name": "Ladyfinger",
        "category": "Vegetable",
        "price": 100,
        "quantity": 0

    },
    {
        "name": "Potato",
        "category": "Vegetable",
        "price": 50,
        "quantity": 0

    },
    {
        "name": "Tomato",
        "category": "Vegetable",
        "price": 30,
        "quantity": 0

    },
    {
        "name": "Onion",
        "category": "Vegetable",
        "price": 35,
        "quantity": 0

    },
    {
        "name": "Garlic",
        "category": "Vegetable",
        "price": 45,
        "quantity": 0

    },
    {
        "name": "Ginger",
        "category": "Vegetable",
        "price": 50,
        "quantity": 0

    },
    {
        "name": "Cauliflower",
        "category": "Vegetable",
        "price": 30,
        "quantity": 0

    },
    {
        "name": "Lettuce",
        "category": "Vegetable",
        "price": 35,
        "quantity": 0

    },
    {
        "name": "Brocolli",
        "category": "Vegetable",
        "price": 40,
        "quantity": 0

    },
    {
        "name": "Capsicum",
        "category": "Vegetable",
        "price": 50,
        "quantity": 0

    }
];

selected_items_name = [];
selected_items_quantity = [];
selected_items_category = [];
selected_items_MRP = [];
selected_items_price = [];

var quantity;
var number;
var sum = 0;
var variable;

function searchItem() {
    var item = document.getElementById("searched_item").value;

    for (var i = 0; i < grocery.length; i++) {
        if (item.toLowerCase() === grocery[i].name.toLowerCase()) {

            var table = document.getElementById("myTable");

            var row = table.insertRow(-1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            var cell5 = row.insertCell(4);
            var cell6 = row.insertCell(5);

            cell1.innerHTML = grocery[i].name;
            cell2.innerHTML = grocery[i].category;
            cell3.innerHTML = grocery[i].price;
            cell4.innerHTML = '<button id="plus" type="button" onclick="quant(1)">+</button>';
            cell5.innerHTML = '<button id="plus" type="button" onclick="quant(-1)">-</button>';
            quant = function (variable) {
                sum = sum + variable;
                if (sum >= 0) {
                    number = sum;
                    cell6.innerHTML = number;
                }
            }

            selected_items_name.push(grocery[i].name);
            selected_items_category.push(grocery[i].category);
            selected_items_MRP.push(grocery[i].price);
            selected_items_quantity.push(number);

            number = 0;
            sum = 0;


            // PROMISE DOESNT WRKIN

            // cell4.innerHTML = '<button id="plus" type="button" onclick="promiseToIncrement()">+</button>';
            // cell5.innerHTML = '<button id="plus" type="button" onclick="quant(-1)">-</button>';

            // var promiseToIncrement = function(){
            //                                     new Promise( function(resolve , reject)
            //                                                 { 
            //                                                      resolve( 1 );
            //                                                 }
            //                                                );
            //                                    }.then( function( fromResolve ){sum = sum + fromResolve;
            //                                                  cell6.innerHTML = sum; } );

            // PROMISE DOESNT WRKIN
        }
    }
}


function addToCart() {
    selected_items_quantity.shift();
    selected_items_name.splice(-1);
    selected_items_category.splice(-1);
    selected_items_MRP.splice(-1); 
 
    console.log( selected_items_MRP);
    for (var i = 0; i < selected_items_name.length; i++) {
        console.log(10);
        var table = document.getElementById("cart");
        var row = table.insertRow(-1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        cell1.innerHTML = selected_items_name[i];
        cell2.innerHTML = selected_items_quantity[i];
    }
}

function checkOut(){
    
     for (var i = 0; i < selected_items_name.length; i++)
    {
        var table = document.getElementById("check_out_table");

            var row = table.insertRow(-1);
            var cella = row.insertCell(0);
            var cellb = row.insertCell(1);
            var cellc = row.insertCell(2);
            var celld = row.insertCell(3);
            var celle = row.insertCell(4);

            cella.innerHTML = selected_items_name[i];
            cellb.innerHTML = selected_items_category[i];
            cellc.innerHTML = selected_items_MRP[i];
            celld.innerHTML = selected_items_quantity[i];
            selected_items_price[i] = selected_items_MRP[i] * selected_items_quantity[i];
            celle.innerHTML = selected_items_price[i];

    }       
}

